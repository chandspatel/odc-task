<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(["namespace" => "Auth", "prefix" => "admin"], function(){

	Route::get('login', 'Admin\LoginController@showLoginForm')->name('admin.login');
	Route::post('login', 'Admin\LoginController@login');
	Route::post('logout', 'Admin\LoginController@logout')->name('admin.logout');

	// Password Reset Routes...
	Route::get('password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
	Route::post('password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	Route::get('password/reset/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
	Route::post('password/reset', 'Admin\ResetPasswordController@reset');

});

Route::group(["prefix" => "admin"], function(){
	Route::get('/', 'DashboardController@index')->name('admin.home');
	Route::get('dashboard', 'DashboardController@index')->name('admin.home');
	Route::get('admin-list', 'AdminController@index')->name('admin.admin_list');
	Route::get('product-category', 'CategoryController@index')->name('category.product_category');
	Route::get('user-list', 'UserController@index')->name('admin.user');


	Route::get('admin-add', 'AdminController@create')->name('admin.admin_add');
	Route::post('admin-store', 'AdminController@store')->name('admin.admin_store');
	Route::get('admin-edit/{id}', 'AdminController@edit')->name('admin.admin_edit');
	Route::get('admin-delete/{id}', 'AdminController@delete')->name('admin.admin_delete');

	Route::get('category-add', 'CategoryController@create')->name('category.category_add');
	Route::post('category-store', 'CategoryController@store')->name('category.category_store');
	Route::get('category-edit/{id}', 'CategoryController@edit')->name('category.category_edit');
	Route::get('category-delete/{id}', 'CategoryController@delete')->name('category.category_delete');

	Route::get('product', 'ProductController@index')->name('admin.product');
	Route::get('product-add', 'ProductController@create')->name('product.product_add');
	Route::post('product-store', 'ProductController@store')->name('product.product_store');
	Route::get('product-edit/{id}', 'ProductController@edit')->name('product.product_edit');
	Route::get('product-delete/{id}', 'ProductController@delete')->name('product.product_delete');
	Route::get('product-download', 'ProductController@download')->name('product.download');


	Route::get('user-add', 'UserController@create')->name('user.user_add');
	Route::post('user-store', 'UserController@store')->name('user.user_store');
	Route::get('user-edit/{id}', 'UserController@edit')->name('user.user_edit');
	Route::get('user-delete/{id}', 'UserController@delete')->name('user.user_delete');

});



Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/products', 'UserProductsController@index')->name('products');
Route::post('/add-to-cart', 'UserProductsController@addtoCart')->name('add_to_cart');
Route::get('/cart', 'CartController@index')->name('cart');
Route::post('/remove-cart', 'CartController@removeCart')->name('remove_cart');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
