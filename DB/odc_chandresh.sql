-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2021 at 01:01 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `odc_chandresh`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `first_name`, `last_name`, `email`, `role_id`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'chands', 'patel', 'chandspatel888@gmail.com', 1, '$2y$10$8EWCdycEQlur/5Uu62HAQukWguH5udDv2/TT0AllYR1yV7ygjrG/u', 'fE7WkF2R2J8txnqHS9qz3Ks6NskFkDLZWQd56fYBgZXjaiD8qJdU6BvaQtrw', '2021-07-13 05:16:57', '2021-07-14 05:05:09'),
(7, 'test', 'test', 'test@gmail.com', 3, '$2y$10$C9EJ2XRhZXL8lp8aYBVl7eJ64lOEyJ/Lf0vfMHG7e3dDs0jLZmiOe', 'She8uCX7tInJrXaUBC0UaqkfkrLEYJU0l0TjBpu3AVoBJ4wycp3RViOgqc2a', '2021-07-13 08:22:27', '2021-07-14 04:16:12');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(27, 4, 3, '2021-07-14 07:45:10', '2021-07-14 07:45:10');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `discount_percent` double(5,2) NOT NULL DEFAULT 0.00,
  `is_discounted` enum('y','n') NOT NULL DEFAULT 'n',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `discount_percent`, `is_discounted`, `created_at`, `updated_at`) VALUES
(1, 'Electronics', 5.00, 'y', '2021-03-14 10:21:43', '2021-07-14 09:46:31'),
(3, 'fashion', 23.00, 'y', '2021-07-13 15:43:03', '2021-07-14 09:48:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('chandspatel888@gmail.com', '$2y$10$Gqh7wFhUIAwbZ0MahIC/QuLW9gpK6gQpisG8u/5XY.W1Ifa8kwsWa', '2021-07-14 05:13:10');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `price` double(10,2) NOT NULL DEFAULT 0.00,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `image`, `category_id`, `price`, `created_at`, `updated_at`) VALUES
(3, 'Samsung Galaxy M42 5G (Prism Dot Black, 8GB RAM, 128GB Storage)', '48MP+8MP+5MP+5MP Quad camera setup-48MP (F 1.8) main camera + 8MP (F2.2) Ultra wide camera+ 5MP (F2.4) depth camera + 5MP (2.4) Macro Camera| 20MP (F2.2) front camera', '2921663611.jpg', 1, 1200.00, '2021-07-13 16:25:37', '2021-07-14 09:49:36'),
(4, 'new product', 'dasd asd asd asd as', '6932126166.png', 3, 50.00, '2021-07-13 17:11:03', '2021-07-14 05:13:42'),
(7, 'new sadasd', 'ashd jasd', '2613392626.jpg', 1, 45.00, '2021-07-14 05:13:52', '2021-07-14 09:50:52');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Root admin', '2021-07-13 18:39:29', '2021-07-13 18:39:29'),
(2, 'Super admin', '2021-07-13 18:39:29', '2021-07-13 18:39:29'),
(3, 'sub admin', '2021-07-13 18:39:29', '2021-07-13 18:39:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'chands', 'sad', 'chandspatel888@gmail.com', '$2y$10$hAnz0OtaXk2gCk9.fiIG7umR1DMo.NBAzmupjhE9PaDXbjO76UsCu', 'MUH00c04cOcq4SnlUF79qBouTKfyFd33RgCT1lBCfC1viZWlVdOFfjNpBz0F', '2021-07-13 05:00:20', '2021-07-14 04:42:23'),
(4, 'new', 'new', 'new@gmail.com', '$2y$10$drvg0sWAR/4PNS.A39J22.CY4T1O5v4S0huMfhiXuhfW7ferbfFPG', '046TaWSzfqth0DapHhnDk5umk3DnGKHqNd0zIv80UJkPUavZcfZyc7QNfXrV', '2021-07-14 02:07:59', '2021-07-14 02:07:59'),
(5, 'new user', 'new name', 'chadns@gmail.com', '$2y$10$4AoRP8AE/VeZdmnjH65WqusAvIGU4/qIC2k/mr4sHV5z00mywbn.m', 'Dpzl8EbHMN8yG0WIOrtpO3DU7VaGZ5GRtoJRnX4xvmLtZXOrY5B5VAle91fm', '2021-07-14 02:57:02', '2021-07-14 02:57:02'),
(6, 'new1', 'new2', 'new@gmail.coms', '$2y$10$KCNmR6sLG1i9N30XY1IITepB7c5AuTBvBXdjHwrNac9k4N5U7x/ru', NULL, '2021-07-14 05:27:26', '2021-07-14 05:27:26'),
(7, 'n', 'n', 'n@gmail.com', '$2y$10$JZFa2/eQR510VSSWXa.VxOrmbv6oWfcv2pIW0x7CZw7jHkt9noolW', NULL, '2021-07-14 05:28:01', '2021-07-14 05:28:01'),
(8, 'dsadasd', 'dsad', 'sdsa@asdas.dsad', '$2y$10$fHl/nhuRf6sQM3/SbFu41ef/6NGiy/2ibsCsgtnEn.qxZZiooiB6O', '4qwsn8PE6h3eSIIBo6ia7emGiXd9G1alkAcHNSFlQCf6uIE0y2IQz5PBjaVc', '2021-07-14 05:30:18', '2021-07-14 05:30:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
