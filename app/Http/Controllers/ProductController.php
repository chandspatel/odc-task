<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\Roles;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Image;
use PDF;

class ProductController extends Controller
{
	const PAGE_LIMIT = 5;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{

		$category = Category::pluck('name', 'id')->toArray();

		$products = Product::join('category as c', 'c.id', '=', 'products.category_id')->select('products.*','c.name as category_name');
		if(!empty($request->name)) {
			$products->where('products.name', 'LIKE', '%'.$request->name.'%');
		}
		if(!empty($request->category_id)) {
			$products->where('c.id', $request->category_id);
		}

		$products = $products->paginate(self::PAGE_LIMIT);

		return view('admin.product.product', compact('products', 'category'));
	}

	public function create()
	{
		$category = Category::pluck('name', 'id')->toArray();
		return view('admin.product.create', compact('category'));
	}

	public function store(Request $request)
	{


		$id = !empty($request->id) ? $request->id : '';

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'price' => 'required|numeric',
			'category_id' => 'required',
			'description' => 'required',
		]);
		if ($validator->fails()) {
			if(!empty($id)) {
				return redirect()->route('product.product_edit', ['id' => $id])->withErrors($validator)->withInput();
			} else {
				return redirect()->route('product.product_add')->withErrors($validator)->withInput();
			}
		}

		$productData = [
			'name' => $request->name,
			'category_id' => $request->category_id,
			'price' => $request->price,
			'description' => $request->description
		];

		if($request->hasFile('image')) {
			$image = $request->file('image');
			$randomFile = str_shuffle(time());
			$imageName = $randomFile.'.'.$image->getClientOriginalExtension();

			$imageNameSmall = 'small_'.$imageName;
			$destinationPath = public_path('/images');

			$img = Image::make($image)->resize(250,250);
			$img->save($destinationPath.'/'.$imageName);

			$img = Image::make($image)->resize(100,100);
			$img->save($destinationPath.'/'.$imageNameSmall);
			$productData['image'] = $imageName;
		}



		if(!empty($id)) {
			product::where('id', $id)->update($productData);
		} else {
			product::create($productData);
		}
		return redirect()->route('admin.product');

	}

	public function edit($id = 0)
	{
		$product = product::where('id', $id)->first();

		if(empty($product)) {
			return redirect()->back();
		}

		$category = Category::pluck('name', 'id')->toArray();

		return view('admin.product.create', compact('product', 'category'));
	}

	public function delete($id = 0)
	{
		product::where('id', $id)->delete();
		return redirect()->route('admin.product');
	}


	public function download(Request $request)
	{

		$products = Product::join('category as c', 'c.id', '=', 'products.category_id')->select('products.*','c.name as category_name');
		if(!empty($request->name)) {
			$products->where('products.name', 'LIKE', '%'.$request->name.'%');
		}
		if(!empty($request->category_id)) {
			$products->where('c.id', $request->category_id);
		}

		$products = $products->get()->toArray();
		$pdf = PDF::loadView('admin.product.product_pdf', ['products' => $products]);

		return $pdf->download('product_list.pdf');
	}

}
