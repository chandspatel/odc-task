<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Roles;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
	const PAGE_LIMIT = 5;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$categories = Category::paginate(self::PAGE_LIMIT);
		//print_r($categories->toArray());exit;
		return view('admin.category.category', compact('categories'));
	}

	public function create()
	{
		return view('admin.category.create');
	}

	public function store(Request $request)
	{

		$id = !empty($request->id) ? $request->id : '';

		$validator = Validator::make($request->all(), [
			'category_name' => 'required|unique:category,name,'.$id,
		]);
		if ($validator->fails()) {
			if(!empty($id)) {
				return redirect()->route('category.category_edit', ['id' => $id])->withErrors($validator)->withInput();
			} else {
				return redirect()->route('category.category_add')->withErrors($validator)->withInput();
			}
		}

		$categoryData = [
			'name' => $request->category_name,
			'is_discounted' => $request->is_discounted,
			'discount_percent' => $request->is_discounted == 'y' ? $request->discount_percent : 0,
		];
		if(!empty($id)) {
			Category::where('id', $id)->update($categoryData);
		} else {
			Category::create($categoryData);
		}
		return redirect()->route('category.product_category');

	}

	public function edit($id = 0)
	{
		$categories = Category::where('id', $id)->first();

		if(empty($categories)) {
			return redirect()->back();
		}
		return view('admin.category.create', compact('categories'));
	}

	public function delete($id = 0)
	{
		Category::where('id', $id)->delete();
		return redirect()->route('category.product_category');
	}
}
