<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Roles;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
	const PAGE_LIMIT = 5;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$admins = Admin::join('roles as r', 'r.id', '=', 'admins.role_id')->select('admins.*','r.id as role_id','r.name as role_name')->paginate(self::PAGE_LIMIT);

		return view('admin.admin_list.admin_list', compact('admins'));
	}

	public function create()
	{
		$roles = Roles::pluck('name', 'id')->toArray();
		return view('admin.admin_list.admin_create', compact('roles'));
	}

	public function store(Request $request)
	{

		$id = !empty($request->id) ? $request->id : '';

		$validator = Validator::make($request->all(), [
			'email' => 'required|email|unique:admins,email,'.$id,
			'first_name' => 'required',
			'last_name' => 'required',
			'password' => 'required',
			'role_id' => 'required',
		]);
		if ($validator->fails()) {
			if(!empty($id)) {
				return redirect()->route('admin.admin_edit', ['id' => $id])->withErrors($validator)->withInput();
			} else {
				return redirect()->route('admin.admin_add')->withErrors($validator)->withInput();
			}
		}

		$adminData = [
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'email' => $request->email,
			'role_id' => $request->role_id,
			'password' => Hash::make($request->password)
		];
		if(!empty($id)) {
			Admin::where('id', $id)->update($adminData);
		} else {
			Admin::create($adminData);
		}
		return redirect()->route('admin.admin_list');

	}

	public function edit($id = 0)
	{
		$admin = Admin::where('admins.id', $id)->join('roles as r', 'r.id', '=', 'admins.role_id')->select('admins.*','r.id as role_id','r.name as role_name')->first();

		if(empty($admin)) {
			return redirect()->back();
		}
		$roles = Roles::pluck('name', 'id')->toArray();
		return view('admin.admin_list.admin_create', compact('roles', 'admin'));
	}

	public function delete($id = 0)
	{

		Admin::where('id', $id)->delete();
		return redirect()->route('admin.admin_list');
	}
}
