<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\Cart;

class CartController extends Controller
{

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth:user');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{

		/*$products = Cart::join('users as u', 'u.id', '=', 'cart.user_id')
			->join('products as p', 'p.id', '=', 'cart.product_id')
			->join('category as c', 'c.id', '=', 'products.category_id')
			->select('products.*','c.name as category_name','c.is_discounted','c.discount_percent');*/
		$products = Cart::leftjoin('users as u', 'u.id', '=', 'cart.user_id')
			->leftjoin('products as p', 'p.id', '=', 'cart.product_id')
			->leftjoin('category as c', 'c.id', '=', 'p.category_id')
			->select('cart.*', 'u.first_name','c.name as category_name','c.is_discounted','c.discount_percent','p.name as product_name', 'p.price','p.image','p.description');
		$products->where('cart.user_id', \Auth::user()->id);

		$products = $products->get();

		//aa($products->toArray());
		return view('user.cart', compact('products'));
	}

    // Remove product to cart
	public function removeCart(Request $request)
    {
        if(empty($request->cart_id)) {
            return response()->json(['code' => 100, 'msg' => 'Something went wrong!']);
        }

        $cartId = $request->cart_id;
        $userId = \Auth::user()->id;

        $cartData = [
            'id' => $cartId,
        ];
        Cart::where($cartData)->delete();
        $total_price = !empty($request->total_price) ? $request->total_price : 0;
        $cart_price = !empty($request->cart_price) ? $request->cart_price: 0;
        $final_price = $total_price - $cart_price;
        $display_price = env('CURRENCY_SYMBOL').number_format($total_price - $cart_price, 2);

        return response()->json(['code' => 200,'final_price' => $final_price, 'display_price' =>$display_price, 'msg' => 'Product removed from cart successfully!']);

    }


}
