<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\Cart;

class UserProductsController extends Controller
{
    const PAGE_LIMIT = 6;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category = Category::pluck('name', 'id')->toArray();

        $products = Product::join('category as c', 'c.id', '=', 'products.category_id')->select('products.*','c.name as category_name','c.is_discounted','c.discount_percent',);
        if(!empty($request->name)) {
            $products->where('products.name', 'LIKE', '%'.$request->name.'%');
        }
        if(!empty($request->category_id)) {
            $products->where('c.id', $request->category_id);
        }

        $products = $products->paginate(self::PAGE_LIMIT);

        $productCart = Cart::leftjoin('users as u', 'u.id', '=', 'cart.user_id')
            ->leftjoin('products as p', 'p.id', '=', 'cart.product_id')
            ->leftjoin('category as c', 'c.id', '=', 'p.category_id')
            ->select('cart.*');
        $productCart->where('cart.user_id', \Auth::user()->id);
        $productCart = $productCart->pluck('product_id', 'id')->toArray();
        return view('user.product', compact('products', 'category', 'productCart'));
    }

    // Add to cart
    public function addtoCart(Request $request)
    {
        if(empty($request->product_id)) {
            return response()->json(['code' => 100, 'msg' => 'Something went wrong!']);
        }

        $productId = $request->product_id;
        $userId = \Auth::user()->id;

        $cartData = [
            'user_id' => $userId,
            'product_id' => $productId,
        ];
        $isCart = Cart::where($cartData)->first();

        if(!empty($isCart)) {
            return response()->json(['code' => 100, 'msg' => 'Product already added to cart!']);
        }
        $cartModel = Cart::create($cartData);
        return response()->json(['code' => 200, 'cart_id' => $cartModel->id, 'msg' => 'Product added to cart successfully!']);

    }


}
