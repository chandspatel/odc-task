<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Roles;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
	const PAGE_LIMIT = 5;
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$users = User::paginate(self::PAGE_LIMIT);
		return view('admin.user.user', compact('users'));
	}

	public function create()
	{
		return view('admin.user.create');
	}

	public function store(Request $request)
	{

		$id = !empty($request->id) ? $request->id : '';

		$validator = Validator::make($request->all(), [
			'email' => 'required|email|unique:users,email,'.$id,
			'first_name' => 'required',
			'last_name' => 'required',
			'password' => 'required',
		]);
		if ($validator->fails()) {
			if(!empty($id)) {
				return redirect()->route('user.user_edit', ['id' => $id])->withErrors($validator)->withInput();
			} else {
				return redirect()->route('user.user_add')->withErrors($validator)->withInput();
			}
		}

		$adminData = [
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'email' => $request->email,
			'password' => Hash::make($request->password)
		];
		if(!empty($id)) {
			User::where('id', $id)->update($adminData);
		} else {
			User::create($adminData);
		}
		return redirect()->route('admin.user');

	}

	public function edit($id = 0)
	{
		$user = User::where('id', $id)->first();

		if(empty($user)) {
			return redirect()->back();
		}
		return view('admin.user.create', compact('user'));
	}

	public function delete($id = 0)
	{
		User::where('id', $id)->delete();
		return redirect()->route('admin.user');
	}
}
