<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
    	'first_name',
    	'last_name',
    	'email',
    	'role_id',
    	'password',
    ];
    protected $table = 'admins';
    protected $primaryKey = 'id';

     public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

}
