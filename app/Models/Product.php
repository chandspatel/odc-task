<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = [
    	'name',
    	'image',
    	'category_id',
    	'price',
    	'description'
    ];
    protected $table = 'products';
    protected $primaryKey = 'id';
}
