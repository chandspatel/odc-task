<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = [
    	'name',
    	'discount_percent',
    	'is_discounted',
    ];
    protected $table = 'category';
    protected $primaryKey = 'id';


}
