<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
			{{ config('app.name', 'Laravel') }}
		</a>
		@php
    		$routeName =  Route::currentRouteName();
		@endphp
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-end" id="navbarsExampleDefault">
            <ul class="navbar-nav m-auto">
            	@if($routeName != 'admin.login')

					@guest
						<li class="nav-item">
							<a class="nav-link" href="{{route('login')}}">Login |</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{route('register')}}">Register</a>
						</li>
					@else
					<li class="nav-item">
	                	<a class="nav-link" href="{{route('products')}}">Products |</a>
	                </li>
					<li class="nav-item">
						<a class="nav-link" href="{{route('cart')}}">My Cart |</a>
					</li>
					<li class="nav-item">
							<a class="nav-link" href="{{ route('logout') }}"
								onclick="event.preventDefault();
										 document.getElementById('logout-form').submit();">
								Logout
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
					@endguest
				@endif

            </ul>

        </div>
    </div>
</nav>