@extends('admin.layouts.app')

@section('content')
@php
	$action = route('category.category_store');
@endphp

<div class="row">
	<div class="col-md-6">
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<h2> {{!empty($categories) ? 'Edit' : 'Create'}} Category</h2>

		<form class="form-horizontal" action="{{$action}}" method="POST">
			{{csrf_field()}}

			@if(!empty($categories))
				<input type="hidden" name="id" value="{{$categories->id}}">
			@endif
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Category Name:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  placeholder="Enter category name" name="category_name" value="{{!empty( old('name')) ? old('name') : (!empty($categories->name) ? $categories->name : "" )}}">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Is Discounted Offer:</label>
				<div class="col-sm-10">
					<label class="radio-inline">
						<input  class="discount_type" type="radio" value="y" id="yes_discount" name="is_discounted" {{!empty($categories->is_discounted) && $categories->is_discounted == 'y' ? 'checked' : 'checked'}} >Yes
					</label>
					<label class="radio-inline">
						<input class="discount_type"  type="radio" value="n" id="no_discount" name="is_discounted" {{!empty($categories->is_discounted) && $categories->is_discounted == 'n' ? 'checked' : ''}}>No
					</label>
				</div>
			</div>

			<div class="form-group" id="hide_discount_percent" style="{{!empty($categories->is_discounted) && $categories->is_discounted == 'n' ? 'display: none;' : '' }} ">
				<label class="control-label col-sm-2" for="email">Discount Percent:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  placeholder="Enter discount percent" name="discount_percent" value="{{!empty( old('discount_percent')) ? old('discount_percent') : (!empty($categories->discount_percent) ? $categories->discount_percent : "" )}}">
				</div>
			</div>


			<div class="form-group">
			  <div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary">Submit</button>
				<a href="{{route('category.product_category')}}" class="btn btn-default">Cancel</a>
			  </div>
			</div>
		  </form>

	</div>

</table>

</div>
@endsection

@section('scripts')
<script type="text/javascript">
		$(document).ready(function(){
			// /$("#hide_discount_percent").hide();
			$(".discount_type").change(function(){
				if($(this).val() == 'y'){
					$("#hide_discount_percent").show();
				} else {
					$("#hide_discount_percent").hide();
				}
			});

			//$(".discount_type").trigger('change');
		});
</script>
@endsection
