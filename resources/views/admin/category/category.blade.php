@extends('admin.layouts.app')

@section('content')

@php
	$categoryData = $categories->toArray();
	$categoryData = !empty($categoryData['data']) ? $categoryData['data'] : [];

@endphp
<div class="row">
	<div class="col-md-12">
		<a href="{{route('category.category_add')}}" class="btn btn-primary btn-xs">Add Category</a>

		<table class="table table-striped">
			<thead>
			  <tr>
				<th>Category Name</th>
				<th>Is Discounted</th>
				<th>Discount Percent</th>
				<th>Action</th>
			  </tr>
			</thead>
			<tbody>
			  @foreach($categoryData as $category)
			  <tr>
				<td>{{$category['name']}}</td>
				<td>{{ucfirst($category['is_discounted'])}}</td>
				<td>{{$category['discount_percent']}}%</td>
				<td>
					<a href="{{route('category.category_edit', ['id' => $category['id']])}}" class="btn btn-primary btn-xs">Edit</a>
					<a onclick="return confirm('Are you sure you want to delete this record?')" href="{{route('category.category_delete', ['id' => $category['id']])}}" class="btn btn-primary btn-xs">Delete</a>
				</td>
			  </tr>
			  @endforeach

			</tbody>

	</div>

</table>
	{{ $categories->links() }}

</div>
@endsection
