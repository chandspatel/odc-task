@extends('admin.layouts.app')

@section('content')
@php
	$action = route('admin.admin_store');
	/*if(!empty($admin)) {
		$action = route('admin.admin_update');
	}*/
@endphp

<div class="row">
	<div class="col-md-6">
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		  <h2> {{!empty($admin) ? 'Edit' : 'Create'}} Admin</h2>

		<form class="form-horizontal" action="{{$action}}" method="POST">
			{{csrf_field()}}

			@if(!empty($admin))
				<input type="hidden" name="id" value="{{$admin->id}}">
			@endif
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">First Name:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  placeholder="Enter first name" name="first_name" value="{{!empty( old('first_name')) ? old('first_name') : (!empty($admin->first_name) ? $admin->first_name : "" )}}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Last Name:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  placeholder="Enter Last name" name="last_name" value="{{!empty( old('last_name')) ? old('last_name') : (!empty($admin->last_name) ? $admin->last_name : "" )}}">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Email:</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="{{!empty( old('email')) ? old('email') : (!empty($admin->email) ? $admin->email : "" )}}">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Password:</label>
				<div class="col-sm-10">
					<input type="password" class="form-control"  placeholder="Enter password" name="password" value="">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="pwd">Admin Role:</label>
				<div class="col-sm-10">
					<select class="form-control" name="role_id">
						<option value="">Select Role</option>
						@foreach($roles as $rolKey => $role)
							@php
								$selected = "";
								if(!empty($admin->role_id) && $rolKey == $admin->role_id) {
									$selected = "selected";
								}
							@endphp
							<option {{$selected}} value="{{$rolKey}}">{{$role}}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
			  <div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary">Submit</button>
				<a href="{{route('admin.admin_list')}}" class="btn btn-default">Cancel</a>
			  </div>
			</div>
		  </form>

	</div>

</table>

</div>
@endsection
