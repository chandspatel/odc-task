@extends('admin.layouts.app')

@section('content')

@php
	$adminsData = $admins->toArray();
	$adminsData = !empty($adminsData['data']) ? $adminsData['data'] : [];

@endphp
<div class="row">
	<div class="col-md-12">
		<a href="{{route('admin.admin_add')}}" class="btn btn-primary btn-xs">Add Admin</a>

		<table class="table table-striped">
			<thead>
			  <tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Role</th>
				<th>Email</th>
				<th>Action</th>
			  </tr>
			</thead>
			<tbody>
			  @foreach($adminsData as $admin)
			  <tr>
				<td>{{$admin['first_name']}}</td>
				<td>{{$admin['last_name']}}</td>
				<td>{{$admin['role_name']}}</td>
				<td>{{$admin['email']}}</td>
				<td>
					<a href="{{route('admin.admin_edit', ['id' => $admin['id']])}}" class="btn btn-primary btn-xs">Edit</a>

					<a onclick="return confirm('Are you sure you want to delete this record?')" href="{{route('admin.admin_delete', ['id' => $admin['id']])}}" class="btn btn-primary btn-xs">Delete</a>
				</td>
			  </tr>
			  @endforeach

			</tbody>

	</div>

</table>
	{{ $admins->links() }}

</div>
@endsection
