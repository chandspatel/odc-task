<!-- Sidebar  -->
@php
    $routeName =  Route::currentRouteName();
@endphp

<nav id="sidebar">
    <div class="sidebar-header">
        <h3>Hi {{ ucfirst(\Auth()->user()->first_name)}}</h3>
    </div>

    <ul class="list-unstyled components">
        <li class=" {{$routeName == 'admin.home' ? 'active' : ''}} ">
            <a href="{{route('admin.home')}}" >Dashboard</a>
        </li>

        <li class="{{$routeName == 'admin.admin_list' ? 'active' : ''}}">
            <a href="{{route('admin.admin_list')}}" >Add/edit admin</a>
        </li>
        @if(\Auth()->user()->role_id == 1)
        <li class="{{$routeName == 'category.product_category' ? 'active' : ''}}">
            <a href="{{route('category.product_category')}}" >Product Category</a>
        </li>
        @endif
        <li class="{{$routeName == 'admin.product' ? 'active' : ''}}">
            <a href="{{route('admin.product')}}" >Products</a>
        </li>
        <li class="{{$routeName == 'admin.user' ? 'active' : ''}}">
            <a href="{{route('admin.user')}}" >Users</a>
        </li>
    </ul>

</nav>
