@extends('admin.layouts.app')

@section('content')
@php
	$action = route('product.product_store');
@endphp

<div class="row">
	<div class="col-md-6">
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<h2> {{!empty($product) ? 'Edit' : 'Create'}} Product</h2>

		<form class="form-horizontal" action="{{$action}}" method="POST" enctype="multipart/form-data">
			{{csrf_field()}}

			@if(!empty($product))
				<input type="hidden" name="id" value="{{$product->id}}">
			@endif
			<div class="form-group">
				<label class="control-label col-sm-2">Product Name*:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  placeholder="Enter product name" name="name" value="{{!empty( old('name')) ? old('name') : (!empty($product->name) ? $product->name : "" )}}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">Product Description*:</label>
				<div class="col-sm-10">
					<textarea class="form-control"  placeholder="Enter product description" name="description">{{!empty( old('description')) ? old('description') : (!empty($product->description) ? $product->description : "" )}}</textarea>

				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">Product Price*:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  placeholder="Enter product price" name="price" value="{{!empty( old('price')) ? old('price') : (!empty($product->price) ? $product->price : "" )}}">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="pwd">Product Category*:</label>
				<div class="col-sm-10">
					<select class="form-control" name="category_id">
						<option value="">Select Category</option>
						@foreach($category as $catKey => $category)
							@php
								$selected = "";
								if(!empty($product->category_id) && $catKey == $product->category_id) {
									$selected = "selected";
								}
							@endphp
							<option {{$selected}} value="{{$catKey}}">{{$category}}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2">Product Image:</label>
				<div class="col-sm-10">
					<input type="file" class="form-control"  id="image" name="image">
					<div id="product_image_preview">
						@if(!empty($product->image))
							<img width="100" src="{{ getProductImage('small_'.$product['image'])}}">
						@endif
					</div>
				</div>
			</div>


			<div class="form-group">
			  <div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary">Submit</button>
				<a href="{{route('admin.product')}}" class="btn btn-default">Cancel</a>
			  </div>
			</div>
		  </form>

	</div>

</table>

</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).on('change','#image',function(){

		$('#product_image_preview').empty();
		var _this = $(this);
		var allowedFiles = ["jpg", "jpeg", "png"];

		if (this.files) {
			var isValid = true;
			var value = this.files[0].name;
			var size = this.files[0].size;
			console.log(value);
			var extension = value.split('.').pop().toLowerCase();
			if ($.inArray(extension, allowedFiles) < 0) {
				alert("Please select valid image. (e.g. jpg, jpeg, png)");
				_this.val('');
				$('#product_image_preview').empty();
				if (isValid == true) {
					isValid = false;
				}

			} else if (size > 2101546) {
				$('#product_image_preview').empty();
				alert("Image size must be less than 2 MB");
				_this.val('');
				if (isValid == true) {
					isValid = false;
				}
			}

			if (isValid) {
				var reader = new FileReader();
				reader.onload = function(event) {
					$('#product_image_preview').empty();

					$($.parseHTML('<img>')).attr('src', event.target.result).attr('width', '100').attr('height', '100').appendTo('#product_image_preview');
				}
				reader.readAsDataURL(this.files[0]);
			} else {
				$('#product_image_preview').empty();
			}
		}

	});
</script>
@endsection
