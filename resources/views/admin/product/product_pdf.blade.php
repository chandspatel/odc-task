@php
@endphp
<table class="table table-striped">
	<thead>
	  <tr>
		<th>Product Name</th>
		<th>Price</th>
		<th>Category Name</th>
		<th>Image</th>

	  </tr>
	</thead>
	<tbody>
		 @foreach($products as $product)
			<tr>
			<td>{{$product['name']}}</td>
			<td>{{ env('CURRENCY_SYMBOL'). $product['price']}}</td>
			<td>{{$product['category_name']}}</td>
			<td> <img width="100" height="100" src="{{ getProductImage('small_'.$product['image'])}}"></td>
			</tr>
		@endforeach

	</tbody>
</table>