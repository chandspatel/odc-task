@extends('admin.layouts.app')

@section('content')

@php
	$productsData = $products->toArray();
	$productsData = !empty($productsData['data']) ? $productsData['data'] : [];

@endphp
<div class="row">
	<div class="col-md-12">
		<a href="{{route('product.product_add')}}" class="btn btn-primary btn-xs">Add Product</a>
		<a href="{{route('product.download', ['name' => !empty($_GET['name'])? $_GET['name']: '','category_id' => !empty($_GET['category_id'])? $_GET['category_id']: '',])}}" class="btn btn-primary btn-xs">Download Product</a>
		<br>
		<br>
		<form class="form-horizontal" action="{{route('admin.product')}}" method="get">

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-sm-3">Product Name*:</label>
						<div class="col-sm-9">
							<input type="text" class="form-control"  placeholder="Enter product name" name="name" value="{{!empty($_GET['name']) ? $_GET['name'] : ''}}">
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label col-sm-3">Category*:</label>
						<div class="col-sm-9">
							<select class="form-control" name="category_id">
							<option value="">Select Category</option>
							@foreach($category as $catKey => $category)
								@php
									$selected = "";
									if(!empty($_GET['category_id']) && $catKey == $_GET['category_id']) {
										$selected = "selected";
									}
								@endphp
								<option {{$selected}} value="{{$catKey}}">{{$category}}</option>
							@endforeach
						</select>
						</div>
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						<label class="control-label col-sm-3">&nbsp;</label>
						<div class="col-sm-8">
							<button type="submit" class="btn btn-primary btn-md">Search</button>
							<a href="{{route('admin.product')}}" class="btn btn-default btn-md">clear</a>
						</div>
					</div>
				</div>

			</div>
		</form>


		<table class="table table-striped">
			<thead>
			  <tr>
				<th>Product Name</th>
				<th>Price</th>
				<th>Category Name</th>
				<th>Image</th>

				<th>Action</th>
			  </tr>
			</thead>
			<tbody>
			  @foreach($productsData as $product)
			  <tr>
				<td>{{$product['name']}}</td>
				<td>{{env('CURRENCY_SYMBOL').$product['price']}}</td>
				<td>{{$product['category_name']}}</td>
				<td> <img width="100" height="100" src="{{ getProductImage('small_'.$product['image'])}}"></td>
				<td>
					<a href="{{route('product.product_edit', ['id' => $product['id']])}}" class="btn btn-primary btn-xs">Edit</a>
					<a onclick="return confirm('Are you sure you want to delete this record?')" href="{{route('product.product_delete', ['id' => $product['id']])}}" class="btn btn-primary btn-xs">Delete</a>
				</td>
			  </tr>
			  @endforeach

			</tbody>

	</div>

</table>
	{{ $products->appends(request()->query())->links() }}

</div>
@endsection
