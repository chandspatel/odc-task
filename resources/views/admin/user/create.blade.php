@extends('admin.layouts.app')

@section('content')
@php
	$action = route('user.user_store');
@endphp

<div class="row">
	<div class="col-md-6">
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		  <h2> {{!empty($user) ? 'Edit' : 'Create'}} User</h2>

		<form class="form-horizontal" action="{{$action}}" method="POST">
			{{csrf_field()}}

			@if(!empty($user))
				<input type="hidden" name="id" value="{{$user->id}}">
			@endif
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">First Name:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  placeholder="Enter first name" name="first_name" value="{{!empty( old('first_name')) ? old('first_name') : (!empty($user->first_name) ? $user->first_name : "" )}}">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Last Name:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  placeholder="Enter Last name" name="last_name" value="{{!empty( old('last_name')) ? old('last_name') : (!empty($user->last_name) ? $user->last_name : "" )}}">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Email:</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="{{!empty( old('email')) ? old('email') : (!empty($user->email) ? $user->email : "" )}}">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Password:</label>
				<div class="col-sm-10">
					<input type="password" class="form-control"  placeholder="Enter password" name="password" value="">
				</div>
			</div>

			<div class="form-group">
			  <div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default">Submit</button>
				<a href="{{route('admin.user')}}" class="btn btn-default">Cancel</a>
			  </div>
			</div>
		  </form>

	</div>

</table>

</div>
@endsection
