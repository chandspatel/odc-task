@extends('admin.layouts.app')

@section('content')

@php
	$usersData = $users->toArray();
	$usersData = !empty($usersData['data']) ? $usersData['data'] : [];

@endphp
<div class="row">
	<div class="col-md-12">
		<a href="{{route('user.user_add')}}" class="btn btn-primary btn-xs">Add User</a>

		<table class="table table-striped">
			<thead>
			  <tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Action</th>
			  </tr>
			</thead>
			<tbody>
			  @foreach($usersData as $user)
			  <tr>
				<td>{{$user['first_name']}}</td>
				<td>{{$user['last_name']}}</td>
				<td>{{$user['email']}}</td>
				<td>
					<a href="{{route('user.user_edit', ['id' => $user['id']])}}" class="btn btn-primary btn-xs">Edit</a>

					<a onclick="return confirm('Are you sure you want to delete this record?')" href="{{route('user.user_delete', ['id' => $user['id']])}}" class="btn btn-primary btn-xs">Delete</a>
				</td>
			  </tr>
			  @endforeach

			</tbody>

	</div>

</table>
	{{ $users->links() }}

</div>
@endsection
