@extends('layouts.app')

@section('content')
<div class="container">

@php
	$productsData = $products->toArray();
	$productsData = !empty($productsData['data']) ? $productsData['data'] : [];

@endphp
	<div class="row">
		<div class="col-12 col-sm-3">
			<div class="card bg-light mb-3">
				<div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i> Categories</div>
				<ul class="list-group category_block">
					@foreach($category as $catKey => $category)
						@php
							$selected = "";
							if(!empty($_GET['category_id']) && $catKey == $_GET['category_id']) {
								$selected = "active";
							}
						@endphp

						<li class="list-group-item {{$selected}}"><a href="{{route('products', ['category_id' => $catKey, 'name' => !empty($_GET['name']) ? $_GET['name'] : ''])}}">{{$category}}</a></li>
					@endforeach
				</ul>
			</div>
			<div class="card bg-light mb-3">
				<div class="card-header bg-success text-white text-uppercase">Search Product</div>
				<div class="card-body">
				<form class="" action="{{route('products')}}" method="" >
					@if(!empty($_GET['category_id']))
						<input type="hidden" name="category_id" value="{{$_GET['category_id']}}">
					@endif
					<div class="form-group">
						<input type="text" class="form-control"  placeholder="Product name" name="name" value="{{!empty($_GET['name']) ? $_GET['name'] : "" }}">
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						@if(!empty($_GET))
							<a href="{{route('products')}}" class="btn btn-default">Clear search</a>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="col">
			@if(!empty($productsData))

				<div class="row">
					@foreach($productsData as $product)
						@php
							$original_price = $product['price'];
							if($product['is_discounted'] == 'y'){
								$discount_percent = $product['discount_percent'];
								$discount_price = $product['price'] -( $product['price'] * $discount_percent /100);
							} else {
								$discount_price = $product['price'];
							}

						@endphp
						<div class="col-12 col-md-6 col-lg-4 product_detail">
							<div class="card">
								<img class="card-img-top" src="{{getProductImage($product['image'])}}" alt="Card image cap">
								<div class="card-body">
									<h4 class="card-title"><a href="#" title="View Product">{{ucfirst($product['name'])}}</a></h4>
									<p class="card-text">{{ucfirst($product['description'])}}</p>
									<div class="row">
										<div class="col">
											<p class="btn btn-danger btn-block">@if($product['is_discounted'] == 'y')<s>{{env('CURRENCY_SYMBOL').number_format($original_price, 2)}}</s>@endif {{env('CURRENCY_SYMBOL').number_format($discount_price, 2)}}</p>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col">

											<a style="{{ in_array($product['id'], $productCart) ? '' :'display: none;' }}" href="javascript:void(0)" class="btn btn-success btn-block remove_cart" data-id="{{array_search ($product['id'], $productCart)}}">Remove cart</a>
											<a style="{{ in_array($product['id'], $productCart) ? 'display: none;' : '' }}" href="javascript:void(0)" class="btn btn-success btn-block add_to_cart" data-id="{{$product['id']}}">Add to cart</a>

										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
				{{ $products->appends(request()->query())->links() }}
			@else
				<div class="row">
					<div class="col-xs-12">
						<h6><strong>No Products Found!</strong></h6>
					</div>
				</div>
			@endif
		</div>

	</div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
	$(document).on('click','.add_to_cart',function(){
		var product_id = $(this).data('id');
		var _this = $(this);
			$.ajax({
				method: "POST",
				url: "{{route('add_to_cart')}}",
				data: { product_id: product_id, '_token' : "{{ csrf_token() }}"},
				dataType: 'json',
				success : function(data) {
					alert(data.msg);
					_this.parents('.product_detail').find('.remove_cart').show();
					_this.parents('.product_detail').find('.remove_cart').data('id', data.cart_id);
					_this.hide();

					/*if(data.code == 200) {
						alert(data.msg);
					}*/
				}
			})
	});

	$(document).on('click','.remove_cart',function(){
		if(confirm('Are you sure you want to remove from cart?')){
			var _this = $(this);
			var cart_id = _this.data('id');
			$.ajax({
				method: "POST",
				url: "{{route('remove_cart')}}",
				data: { cart_id: cart_id, '_token' : "{{ csrf_token() }}"},
				dataType: 'json',
				success : function(data) {
					alert(data.msg);
					_this.parents('.product_detail').find('.add_to_cart').show();
					_this.hide();
				}
			})

		}
	});
</script>
@endsection
