@extends('layouts.app')

@section('content')
<div class="container">

@php
	$productsData = $products->toArray();
	$total_cart_price = 0;
@endphp
	<div class="row">
		<div class="col-xs-8">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">
						<div class="row">
							<div class="col-xs-6">
								<h5> Shopping Cart</h5>
							</div>
							<div class="col-xs-6">
								<a href="{{route('products')}}" class="btn btn-primary btn-sm btn-block">
									 Continue shopping
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
					@if(!empty($productsData))
						@foreach($productsData as $product)
							@php
								$original_price = $product['price'];

								if($product['is_discounted'] == 'y'){
									$discount_percent = $product['discount_percent'];
									$discount_price = $product['price'] -( $product['price'] * $discount_percent /100);
								} else {
									$discount_price = $product['price'];
								}
								$total_cart_price += $discount_price;

							@endphp
							<div class="cart-detail">
								<div class="row">
									<div class="col-xs-2"><img class="img-responsive" src="{{getProductImage('small_'.$product['image'])}}">
									</div>
									<div class="col-xs-4">
										<h4 class="product-name"><strong>{{$product['product_name']}}</strong></h4><h4><small>{{$product['description']}}</small></h4>
									</div>
									<div class="col-xs-6">
										<div class="col-xs-6 text-right">
											<h6>@if($product['is_discounted'] == 'y')<s>{{env('CURRENCY_SYMBOL').number_format($original_price, 2)}}</s>@endif <strong>{{env('CURRENCY_SYMBOL').number_format($discount_price, 2)}} </strong></h6>
										</div>
										<div class="col-xs-2">
											<button type="button" data-price="{{$discount_price}}" class="btn btn-danger remove_cart" data-id="{{$product['id']}}">
												 Remove
											</button>
										</div>
									</div>
								</div>
								<hr>
							</div>
						@endforeach
					@else
						<div class="row">
							<div class="col-xs-2">
								<h6><strong>Cart is empty!</strong></h6>
							</div>
						</div>
					@endif
				</div>
				<div class="panel-footer">
					<div class="row text-center">
						<div class="col-xs-3 col-xs-offset-9">
							<h4 class="text-right" id="total_price" data-price="{{$total_cart_price}}" >Total <strong id="display_price">{{env('CURRENCY_SYMBOL').number_format($total_cart_price, 2)}} </strong></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
	$(document).on('click','.remove_cart',function(){
		if(confirm('Are you sure you want to remove from cart?')){
			var _this = $(this);
			var cart_id = _this.data('id');
			var total_price = $("#total_price").data('price');
			var cart_price = _this.data('price');
			$.ajax({
				method: "POST",
				url: "{{route('remove_cart')}}",
				data: { cart_id: cart_id, '_token' : "{{ csrf_token() }}" ,"total_price" : total_price, "cart_price" : cart_price},
				dataType: 'json',
				success : function(data) {
					alert(data.msg);
					_this.parents('.cart-detail').remove();
					$("#display_price").text(data.display_price);
					$("#total_price").data('price', data.final_price);

					if($('.cart-detail').length <=0){
						window.location.href="{{route('products')}}";
					}
				}
			})

		}
	});
</script>
@endsection
